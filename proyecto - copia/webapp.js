const express = require("express");
const aplicacion = express();
const mysql = require("mysql");
const { Pool } = require("pg");
const bodyParser = require('body-parser')
const session = require("express-session");
const flash = require('express-flash')
var path = require("path")
const fileUpload = require("express-fileupload");
const nodemailer = require("nodemailer");
const XLSX = require("xlsx");

const transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'andresfelipear@gmail.com',
    pass: "Qwe321r4"
  }
})

function enviarCorreoBienvenida(email, nombre) {
  const opciones = {
    from: 'andresfelipear@gmail.com',
    to: email,
    subject: 'Bienvenido a la pagina',
    text: `Hola ${nombre}`
  }
  transporter.sendMail(opciones, (error, info) => {

  })
}

const pool2 = new Pool({
  max: 20,
  host: 'localhost',
  user: 'postgres',
  password: '16971226',
  database: 'cobrando'
})

var pool = mysql.createPool({
  connectionLimit: 20,
  host: 'localhost',
  user: 'root',
  password: 'fn2h2ye2',
  database: 'blog_viajes'
})

aplicacion.set('view engine', 'ejs')
aplicacion.use(bodyParser.json())
aplicacion.use(bodyParser.urlencoded({ extended: false }))
aplicacion.use(express.static('public'))
aplicacion.use(session({ secret: 'token-muy-secreto', resave: true, saveUninitialized: true }));
aplicacion.use(flash())
aplicacion.use(fileUpload());


aplicacion.use('/admin/', (peticion, respuesta, siguiente) => {
  if (!peticion.session.usuario) {
    peticion.flash('mensaje', 'Debe iniciar sesión')
    respuesta.redirect("/")
  }
  else {
    siguiente();
  }
})


aplicacion.listen(8080, function () {
  console.log("Servidor Iniciado");
})

aplicacion.get("/registro", function (req, res) {
  res.render("registro", { mensaje: req.flash('mensaje') });
})

aplicacion.get("/", function (req, res) {
  res.render("sesion", { mensaje: req.flash('mensaje') });
})


aplicacion.post('/procesar_registro', function (peticion, respuesta) {
  pool2.connect(function (err, client, release) {
    if (err) {
      console.log("error conectandose a la base de datos: " + err)
    }
    const email = peticion.body.email.toLowerCase().trim();
    const pseudonimo = peticion.body.pseudonimo.trim();
    const contrasena = peticion.body.contrasena;

    const consultaEmail = `
        SELECT *
        FROM usuarios
        WHERE email='${email}'
      `
    client.query(consultaEmail, function (error, filas, campos) {
      if (filas.rowCount != 0) {
        peticion.flash('mensaje', 'Email duplicado')
        respuesta.redirect('/registro')
      }
      else {

        const consultaPseudonimo = `
            SELECT *
            FROM usuarios
            WHERE pseudonimo = '${pseudonimo}'
          `

        client.query(consultaPseudonimo, function (error, filas, campos) {
          if (filas.rowCount != 0) {
            peticion.flash('mensaje', 'Pseudonimo duplicado')
            respuesta.redirect('/registro')
          }
          else {

            const consulta = `
                                  INSERT INTO
                                  usuarios
                                  (email, contrasena, pseudonimo)
                                  VALUES (
                                    '${email}',
                                    '${contrasena}',
                                    '${pseudonimo}'
                                  )
                                `
            client.query(consulta, function (error, filas, campos) {
              enviarCorreoBienvenida(email, pseudonimo);
              peticion.flash('mensaje', 'Usuario registrado con exito')
              respuesta.redirect('/')
            })
          }
        })
      }
    })
    release()
  })
})

aplicacion.post('/procesar_carga', function (peticion, respuesta) {
  pool2.connect(function (err, client, release) {
    if (peticion.files && peticion.files.excel) {
      const archivoExcel = peticion.files.excel;
      let data = new Uint8Array(archivoExcel.data);
      let workbook = XLSX.read(data, { type: "array" });
      let worksheet = workbook.Sheets[workbook.SheetNames[0]];
      let sheet = XLSX.utils.sheet_to_json(worksheet, { header: 1 });
      console.log("archivo excel: " + sheet);
      let datosExcel = sheet;
      let columnExcel = sheet[0];
      console.log('columnas de excel: ' + columnExcel)
      let columnBaseDatos = [];
      const consulta = `select * from datos`;
      client.query(consulta, function (err, result) {
        for (var i in result.fields) {
          columnBaseDatos[i] = result.fields[i].name;

        }
        console.log('columnas base de datos: ' + columnBaseDatos)
        let columnNews = columnExcel.filter(a => !columnBaseDatos.includes(a.toLowerCase()));
        console.log('columnas nuevas: ' + columnNews)
        if (columnNews != 0) {
          let tipoDato = '';
          for (var i in columnNews) {
            tipoDato += columnNews[i] + ' text, ADD ';
          }
          const consulta2 = `ALTER TABLE datos ADD ${tipoDato} `.trim();
          const consulta3 = consulta2.substring(0, consulta2.length - 5);
          console.log(consulta3);
          client.query(consulta3, function (err, result) {
            if (err) {
              console.log(err);
            }
            else { console.log('alter table ok'); }
          })
        }
        const campos = columnExcel.join(',');
        let campos2 = campos.substring(0, campos.length - 1).trim();
        for (var a in datosExcel) {
          if (a == 0) {
          }
          else {
            let valores = datosExcel[a].join(`','`);
            let valores2 = `'${valores}'`;
            const consulta4 = `insert into datos(${campos}) values(${valores2})`;
            console.log(consulta4)
            client.query(consulta4, function (err, result) {
              if (err) {
                console.log(err);
              }
            })
          }
        }
        peticion.flash('mensaje', 'Carga Exitosa');
        respuesta.redirect('/admin');
      })
      release();
    }
  });
})

aplicacion.get("/admin", function (req, res) {
  pool2.connect(function (err, client, release) {
    const consulta = `select * from datos`;
    console.log("inicio exitoso");
    client.query(consulta, function (err, result2) {
      const columnBaseDatos = [];
      for (var i in result2.fields) {
        columnBaseDatos[i] = result2.fields[i].name;

      }
      let datos = [];
      datos = result2.rows;
      var prueba = datos[0];
      console.log(datos);
      res.render("admin/cargaExcel", { mensaje: req.flash('mensaje'), datos: datos, column: columnBaseDatos, usuario: req.session.usuario });
    })
    release();
  })
});

aplicacion.post("/procesar-inicio", function (req, res) {
  pool2.connect(function (err, client, release) {
    const email = req.body.email;
    const contrasena = req.body.contrasena;
    const consultaEmail = `select * from usuarios where email='${email}'`;
    client.query(consultaEmail, function (error, result, campos) {
      if (result.rowCount != 0) {
        console.log(result.rowCount);
        if (result.rows[0].contrasena == contrasena) {
          req.session.usuario = result.rows[0];
          req.flash('mensaje', 'Bienvenido');
          res.redirect('/admin');
        }
        else {
          req.flash('mensaje', 'Contraseña incorrecta');
          res.redirect('/');
        }
      }

      else {
        req.flash('mensaje', 'Credenciales invalidas');
        res.redirect('/');
      }
    })
    release();
  })
})

aplicacion.get('/procesar_cerrar_sesion', function (peticion, respuesta) {
  peticion.session.destroy();
  respuesta.redirect("/")
});

