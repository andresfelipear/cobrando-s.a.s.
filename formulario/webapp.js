const express = require('express')
const aplicacion = express();
var date = new Date();
aplicacion.listen(8080, function () {
    console.log("Servidor iniciado");
})
aplicacion.set("view engine","ejs");
aplicacion.get("/", function(req, res){
    res.render("pages/index");
});

const bodyParser = require('body-parser');
 
aplicacion.use(bodyParser.json());
aplicacion.use(bodyParser.urlencoded({extended:true}));
aplicacion.post("/procesar",function(req, res){
    let re;
    re = /^[^]{6,}/
    if(!re.test(req.body.usuario)){
        res.send("ERROR");
    }
    re = re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    if(!re.test(req.body.correo)){
        res.send("ERROR");
    }
    res.send("OK");

})
