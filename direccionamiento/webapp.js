const express = require('express')
const aplicacion = express();
aplicacion.listen(8080, function () {
    console.log("Servidor iniciado");
})
const rutaDeportes = require("./rutas/deportes");
const rutaConciertos = require("./rutas/conciertos");
aplicacion.use("/deportes", rutaDeportes);
aplicacion.use("/conciertos",rutaConciertos);

aplicacion.get("/", function(req, res){
    res.send("Pagina principal");
})