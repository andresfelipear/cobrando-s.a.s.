const express =require("express");
const aplicacion = express();
const bodyParser = require('body-parser');
const mysql = require("mysql");

aplicacion.listen(8080, function(){
    console.log("servidor iniciado");
})

aplicacion.use(bodyParser.json());
aplicacion.use(bodyParser.urlencoded({extended:true}));
aplicacion.set("view engine", "ejs");
var pool = mysql.createPool({
    connectionLimit:"20",
    host: "localhost",
    user: "root",
    password: "fn2h2ye2",
    database: "inventario"
  })
aplicacion.get("/", function(req, res){
  pool.getConnection(function(err, connection){
      const query = "select *from productos";
      connection.query(query, function(err, filas, campos){
          res.render('index', {productos:filas});
      });
      connection.release();
  });
}
);

aplicacion.post("/procesar",function(req, res){
    pool.getConnection(function(err,connection){
        const query = `insert into productos(nombre, cantidad, precio) values(${connection.escape(req.body.nombre)},${connection.escape(req.body.cantidad)}, ${connection.escape(req.body.precio)})`;
        connection.query(query, function(err, filas, campos){
            res.redirect("/");
        })
        connection.release();
    })
})
