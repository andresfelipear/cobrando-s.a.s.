const cookieParser = require("cookie-parser");
const express = require("express");
const aplicacion = express();
aplicacion.use(cookieParser());
aplicacion.listen(8080, function () {
    console.log("Servidor iniciado");
})

aplicacion.get("/",function(req, res){
    res.cookie('nombre', req.query.nombre);
    res.send(`Bienvenido ${req.query.nombre}`);
   
})

aplicacion.get("/leer", function(req,res){
    res.send(`Bienvenido: ${req.cookies['nombre']}`);
})