-- MySQL dump 10.13  Distrib 8.0.18, for Win64 (x86_64)
--
-- Host: localhost    Database: blog_viajes
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `autores`
--

DROP TABLE IF EXISTS `autores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `autores` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `contrasena` varchar(255) NOT NULL,
  `pseudonimo` varchar(255) NOT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `autores`
--

LOCK TABLES `autores` WRITE;
/*!40000 ALTER TABLE `autores` DISABLE KEYS */;
INSERT INTO `autores` VALUES (1,'luis@email.com','123123','luis2000',NULL),(2,'ana@email.com','123123','a55555',NULL),(3,'andresfelipear@gmail.com','123456','pipe',NULL),(4,'ingridbarreto13@gmail.com','123','ingrid',NULL),(5,'pipe@email.com','123','pipe1314',NULL),(6,'andres@email.com','123','felipear2013',NULL),(7,'jorge@email.com','123','jorgi',NULL),(8,'david@email.com','123123','davidlo',NULL),(9,'mama@email.com','123','mama',NULL),(10,'juan@email.com','123','juana','10.PNG'),(11,'andres@email.com','123','yperryfelipear','11.jpg'),(13,'andresfelipe.arevalo@hotmail.com','123','andresito2013',NULL),(14,'pepito@email.com','123456','pepito123',NULL);
/*!40000 ALTER TABLE `autores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `publicaciones`
--

DROP TABLE IF EXISTS `publicaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `publicaciones` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) NOT NULL,
  `resumen` varchar(255) NOT NULL,
  `contenido` varchar(255) NOT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `votos` int(10) DEFAULT NULL,
  `fecha_hora` timestamp NULL DEFAULT NULL,
  `autor_id` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_autor_id` (`autor_id`),
  CONSTRAINT `fk_autor_id` FOREIGN KEY (`autor_id`) REFERENCES `autores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `publicaciones`
--

LOCK TABLES `publicaciones` WRITE;
/*!40000 ALTER TABLE `publicaciones` DISABLE KEYS */;
INSERT INTO `publicaciones` VALUES (1,'Roma','Buen viaje a Roma','Contenido',NULL,0,'2018-09-10 06:08:27',1),(2,'Grecia','Buen viaje a Grecia','Contenido</p>',NULL,0,'2018-09-11 06:08:27',1),(3,'Paris','Buen viaje a Paris','Contenido',NULL,0,'2018-09-12 06:08:27',1),(4,'Costa Rica','Buen viaje a Costa Rica','Contenido',NULL,0,'2018-09-13 06:08:27',2),(5,'Mar de Plata','Buen viaje a Mar de Plata','Contenido',NULL,0,'2018-09-14 06:08:27',2),(6,'Guadalajara','Buen viaje a Guadalajara','Contenido',NULL,0,'2018-09-15 06:08:27',2),(7,'China','Buen viaje a China','Contenido',NULL,2,'2018-09-16 06:08:27',2),(10,'Colombianita','Hola Colombia','Nuestro viaje a Colombia',NULL,0,'2020-04-22 05:00:00',3),(11,'Japonecito','hola japon22','mi viaje a japon',NULL,3,'2020-04-22 05:00:00',1),(14,'villavicencio','hola villavicencio','123',NULL,4,'2020-04-22 05:00:00',4),(15,'Bogota D.C.','viajando por bogota','<p>Ciudad caotica pero linda</p>',NULL,7,'2020-04-26 05:00:00',11);
/*!40000 ALTER TABLE `publicaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'blog_viajes'
--

--
-- Dumping routines for database 'blog_viajes'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-05-30 10:24:17
