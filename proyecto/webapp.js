const express = require("express");
const aplicacion = express();
const mysql = require("mysql");
const bodyParser = require('body-parser')
const session = require("express-session");
const flash = require('express-flash')
var path = require("path")
const fileUpload = require("express-fileupload");
const nodemailer = require("nodemailer");

const transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'andresfelipear@gmail.com',
    pass: "Qwe321r4"
  }
})

function enviarCorreoBienvenida(email, nombre) {
  const opciones = {
    from: 'andresfelipear@gmail.com',
    to: email,
    subject: 'Bienvenido al blog de viajes',
    text: `Hola ${nombre}`
  }
  transporter.sendMail(opciones, (error, info) => {

  })
}

var pool = mysql.createPool({
  connectionLimit: 20,
  host: 'localhost',
  user: 'root',
  password: 'fn2h2ye2',
  database: 'blog_viajes'
})

aplicacion.set('view engine', 'ejs')
aplicacion.use(bodyParser.json())
aplicacion.use(bodyParser.urlencoded({ extended: false }))
aplicacion.use(express.static('public'))
aplicacion.use(session({ secret: 'token-muy-secreto', resave: true, saveUninitialized: true }));
aplicacion.use(flash())
aplicacion.use(fileUpload());


aplicacion.use('/admin/', (peticion, respuesta, siguiente) => {
  if (!peticion.session.usuario) {
    peticion.flash('mensaje', 'Debe iniciar sesión')
    respuesta.redirect("/inicio-sesion")
  }
  else {
    siguiente();
  }
})

aplicacion.get("/", function (req, res) {
  pool.getConnection(function (err, connection) {
    let consulta;
    let modificadorConsulta = "";
    let pagina = 0;
    let modificadorPagina = "";
    const busqueda = (req.query.busqueda) ? req.query.busqueda : "";
    if (busqueda != "") {
      modificadorConsulta = `where titulo LIKE '%${busqueda}%' OR resumen LIKE '%${busqueda}%' OR contenido 
      LIKE '%${busqueda}%' `;
      modificadorPagina = "";

    }
    else {
      pagina = (req.query.pagina) ? parseInt(req.query.pagina) : 0;
      if (pagina < 0) {
        pagina = 0;
      }
      modificadorPagina = `LIMIT 5 OFFSET ${pagina * 5}`
    }

    console.log(pagina);
    consulta = `
      select publicaciones.id, titulo, resumen, fecha_hora, pseudonimo, votos, avatar
      FROM publicaciones
      INNER JOIN autores
      ON publicaciones.autor_id = autores.id
      ${modificadorConsulta}
      ORDER BY fecha_hora DESC
      ${modificadorPagina}
      `
    connection.query(consulta, function (error, filas, campos) {
      res.render("index", { publicaciones: filas, busqueda: busqueda, pagina: pagina, mbusqueda: 1, mensaje: req.flash('mensaje') });
    })
    connection.release();
  })
})

aplicacion.listen(8080, function () {
  console.log("Servidor Iniciado");
})

aplicacion.get("/registro", function (req, res) {
  res.render("registro", { mensaje: req.flash('mensaje') });
})

aplicacion.get("/inicio-sesion", function (req, res) {
  res.render("sesion", { mensaje: req.flash('mensaje') });
})


aplicacion.post('/procesar_registro', function (peticion, respuesta) {
  pool.getConnection(function (err, connection) {

    const email = peticion.body.email.toLowerCase().trim();
    const pseudonimo = peticion.body.pseudonimo.trim();
    const contrasena = peticion.body.contrasena;

    const consultaEmail = `
        SELECT *
        FROM autores
        WHERE email = ${connection.escape(email)}
      `

    connection.query(consultaEmail, function (error, filas, campos) {
      if (filas.length > 0) {
        peticion.flash('mensaje', 'Email duplicado')
        respuesta.redirect('/registro')
      }
      else {

        const consultaPseudonimo = `
            SELECT *
            FROM autores
            WHERE pseudonimo = ${connection.escape(pseudonimo)}
          `

        connection.query(consultaPseudonimo, function (error, filas, campos) {
          if (filas.length > 0) {
            peticion.flash('mensaje', 'Pseudonimo duplicado')
            respuesta.redirect('/registro')
          }
          else {

            const consulta = `
                                  INSERT INTO
                                  autores
                                  (email, contrasena, pseudonimo)
                                  VALUES (
                                    ${connection.escape(email)},
                                    ${connection.escape(contrasena)},
                                    ${connection.escape(pseudonimo)}
                                  )
                                `
            connection.query(consulta, function (error, filas, campos) {
              if (peticion.files && peticion.files.avatar) {
                const archivoAvatar = peticion.files.avatar
                const id = filas.insertId
                const nombreArchivo = `${id}${path.extname(archivoAvatar.name)}`
                archivoAvatar.mv(`./public/avatars/${nombreArchivo}`, (error) => {
                  const consultaAvatar = `
                                UPDATE
                                autores
                                SET avatar = ${connection.escape(nombreArchivo)}
                                WHERE id = ${connection.escape(id)}
                              `
                  connection.query(consultaAvatar, (error, filas, campos) => {
                    enviarCorreoBienvenida(email, pseudonimo);
                    peticion.flash('mensaje', 'Usuario registrado con avatar')
                    respuesta.redirect('/registro')
                  })
                })
              }
              else {
                enviarCorreoBienvenida(email, pseudonimo);
                peticion.flash('mensaje', 'Usuario registrado')
                respuesta.redirect('/registro')
              }
            })
          }
        })
      }
    })
    connection.release()
  })
})

aplicacion.post("/procesar-inicio", function (req, res) {
  pool.getConnection(function (err, connection) {
    const email = req.body.email;
    const contrasena = req.body.contrasena;
    const consultaEmail = `select * from autores where email=${connection.escape(email)}`;
    connection.query(consultaEmail, function (error, filas2, campos) {
      if (filas2.length > 0) {
        if (filas2[0].contrasena == contrasena) {
          req.session.usuario = filas2[0];
          res.redirect("/admin/index");

        }
      }

      else {
        req.flash('mensaje', 'Credenciales invalidas');
        res.redirect('/inicio-sesion');
      }
    })
    connection.release();
  })
})

aplicacion.get('/admin/index', function (peticion, respuesta) {
  pool.getConnection(function (err, connection) {
    const query = `select * from publicaciones where autor_id = ${peticion.session.usuario.id}`;
    connection.query(query, function (error, filas, campos) {
      respuesta.render('admin/index', { usuario: peticion.session.usuario, mensaje: peticion.flash('mensaje'), publicaciones: filas })
    })
    connection.release();
  })

})

aplicacion.get('/procesar_cerrar_sesion', function (peticion, respuesta) {
  peticion.session.destroy();
  respuesta.redirect("/")
});

aplicacion.get("/admin/editar/:id", function (req, res) {

  pool.getConnection(function (err, connection) {
    const query = `select * from publicaciones where id = ${connection.escape(req.params.id)}`;
    connection.query(query, function (error, filas, campos) {
      res.render('admin/editar', { usuario: req.session.usuario, mensaje: req.flash('mensaje'), publicacion: filas[0] })
    })
    connection.release();
  })
});

aplicacion.get("/admin/agregar", function (req, res) {
  res.render("admin/agregar", { usuario: req.session.usuario, mensaje: req.flash('mensaje') })
});


aplicacion.get("/admin/procesar_eliminar/:id", function (req, res) {
  pool.getConnection(function (err, connection) {
    connection.query(`delete from publicaciones where id=${connection.escape(req.params.id)}`, function (error, filas, campos) {
      req.flash("mensaje", "Publicacion eliminada");
      res.redirect("/admin/index");
    })
    connection.release();
  })
})



aplicacion.post("/admin/procesar_agregar", function (req, res) {
  pool.getConnection(function (err, connection) {
    const date = new Date();
    const fecha = `${date.getFullYear()}-${date.getMonth()}-${date.getDate()}`;
    const query = `
      insert into publicaciones(titulo, resumen, contenido, fecha_hora, autor_id)
      values
      (
        ${connection.escape(req.body.titulo)},
        ${connection.escape(req.body.resumen)},
        ${connection.escape(req.body.contenido)},
        '${fecha}',
        ${connection.escape(req.session.usuario.id)}
      )
      `;
    connection.query(query, function (error, filas, campos) {
      req.flash("mensaje", "Publicacion Agregada")
      res.redirect("/admin/index")
    })
    connection.release();
  })

});

aplicacion.post("/admin/procesar_editar", function (req, res) {
  pool.getConnection(function (err, connection) {

    const query2 = `select *from publicaciones
    where
    id=${connection.escape(req.body.id)} and autor_id=${req.session.usuario.id}
    `
    connection.query(query2, function (error, fil, camp) {
      if (fil.length > 0) {
        console.log("entro al if");
        const date = new Date();
        const fecha = `${date.getFullYear()}-${date.getMonth()}-${date.getDate()}`;
        const query = `
          update publicaciones
          set
            titulo=${connection.escape(req.body.titulo)},
            resumen=${connection.escape(req.body.resumen)},
            contenido=${connection.escape(req.body.contenido)},
            fecha_hora='${fecha}'
          where id=${connection.escape(req.body.id)}  
          `;
        console.log(query);
        connection.query(query, function (error, filas, campos) {
          req.flash("mensaje", "Publicacion Agregada")
          res.redirect("index")
        })
      }
      else {
        req.flash("mensaje", "La Publicacion que esta tratando de editar no es de su autoria");
        res.redirect("index");
      }
    })

    connection.release();
  })

});


aplicacion.get("/publicacion/:id", function (req, res) {
  pool.getConnection(function (err, connection) {
    const query = `select *from publicaciones where id=${connection.escape(req.params.id)}`;
    connection.query(query, function (error, filas, campos) {
      if (filas.length > 0) {
        res.render("detalle", { publicacion: filas[0], mensaje: req.flash('mensaje') });
      }
      else {
        res.redirect("/");
      }
    })
    connection.release();
  })

})

aplicacion.get('/autores', (peticion, respuesta) => {
  pool.getConnection((err, connection) => {
    const consulta = `
      SELECT autores.id id, pseudonimo, avatar, publicaciones.id publicacion_id, titulo
      FROM autores
      INNER JOIN
      publicaciones
      ON
      autores.id = publicaciones.autor_id
      ORDER BY autores.id DESC, publicaciones.fecha_hora DESC
    `
    connection.query(consulta, (error, filas, campos) => {
      autores = []
      ultimoAutorId = undefined
      filas.forEach(registro => {
        if (registro.id != ultimoAutorId) {
          ultimoAutorId = registro.id
          autores.push({
            id: registro.id,
            pseudonimo: registro.pseudonimo,
            avatar: registro.avatar,
            publicaciones: []
          })
        }
        autores[autores.length - 1].publicaciones.push({
          id: registro.publicacion_id,
          titulo: registro.titulo
        })
      });
      respuesta.render('autores', { autores: autores })
    })


    connection.release()
  })
})

aplicacion.get("/publicacion/:id/votar", function (req, res) {
  pool.getConnection(function (error, connection) {
    connection.query(`select *from publicaciones where id=${connection.escape(req.params.id)}`, function (err, filas, campos) {
      if (filas.length > 0) {
        const query = `update publicaciones set votos=votos+1 where id=${connection.escape(req.params.id)}`;
        console.log(query);
        connection.query(query, function (err, filas, campos) {
          res.redirect(`/publicacion/${req.params.id}`);
        })
      }
      else {
        req.flash('mensaje', 'publicacion invalida')
        res.redirect('/');
      }
    })
    connection.release();
  })
})


aplicacion.get("/api/v1/publicaciones", function (req, res) {
  pool.getConnection(function (error, connection) {
    const palabra = (req.query.busqueda) ? req.query.busqueda : "";
    let modificador = "";
    if (palabra != "") {
      modificador = `where titulo LIKE '%${palabra}%' OR resumen LIKE '%${palabra}%' OR contenido LIKE '%${palabra}%'`
    }
    console.log(palabra);
    const query = `select *from publicaciones ${modificador} `;
    connection.query(query, function (err, filas, campos) {
      console.log(filas);
      res.json({ data: filas });
    })
    connection.release();
  })
})

aplicacion.get("/api/v1/publicaciones/:id", function (req, res) {

  pool.getConnection(function (error, connection) {
    const query = `select *from publicaciones where id=${req.params.id}`;
    connection.query(query, function (err, filas, campos) {
      if (filas.length > 0) {
        res.json({ data: filas[0] });
      }
      else {
        res.status(404);
        res.send({ errors: "Publicacion no encontrada" });
      }
    })
  })

})

aplicacion.get("/api/v1/autores", function(req,res){
  pool.getConnection(function(error,connection){
    const query = `select *from autores`;
    connection.query(query, function(err,filas,campos){
      res.json({data: filas});
    })
  })
})

aplicacion.post("/api/v1/autores", function (req, res) {
  pool.getConnection(function (error, connection) {
    const email = req.body.email.toLowerCase().trim();
    const pseudonimo = req.body.pseudonimo.trim();
    const contrasena = req.body.contrasena;
    const consultaEmail = `
    SELECT *
    FROM autores
    WHERE email = ${connection.escape(email)}
  `

    connection.query(consultaEmail, function (error, filas, campos) {
      if (filas.length > 0) {
        res.status(502);
        res.send("El email ya se encuentra registrado")
      }
      else {

        const consultaPseudonimo = `
        SELECT *
        FROM autores
        WHERE pseudonimo = ${connection.escape(pseudonimo)}
      `

        connection.query(consultaPseudonimo, function (error, filas, campos) {
          if (filas.length > 0) {
            res.status(502);
            res.send("El pseudonimo ya se encuentra registrado");
          }
          else {

            const consulta = `
                              INSERT INTO
                              autores
                              (email, contrasena, pseudonimo)
                              VALUES (
                                ${connection.escape(email)},
                                ${connection.escape(contrasena)},
                                ${connection.escape(pseudonimo)}
                              )
                            `
            connection.query(consulta, function (error, filas, campos) {
                const nuevoId = filas.insertId;
                const query2 = `select *from autores where id=${nuevoId}`;
                connection.query(query2,function(err,fil,camp){
                  res.status(201);
                  res.json({data: fil[0]});
                })
                
            })
          }
        })
      }
    })
    connection.release();
  })

})

aplicacion.post("/api/v1/publicaciones", function(req, res){
  pool.getConnection(function(error,connection){
    const email = req.query.email;
    const contrasena = req.query.contrasena;
    const query = `select * from autores where email=${connection.escape(email)}`;
    console.log(query);
    console.log(contrasena);
    connection.query(query, function(err,filas,campos){
      if(filas >0){
          if(filas[0].contrasena == contrasena){
            const query2 = `insert into publicaciones(titulo,resumen,contenido,autor_id)
            values(${req.body.titulo}, 
              ${req.body.resumen},
              ${req.body.contenido},
              ${filas[0].id})
            `
            connection.query(query2, function(err,filas,campos){
              connection.query(`select *from publicaciones where id=${filas.insertId}`, function(err,filas,campos){
                res.status(201);
                res.json({data:filas[0]});
              })
            })
          }
          else{
            res.status(401);
            res.send("contrasena incorrecta");
          }
      }
      else{
        res.status(404);
        res.send("Email no registrado")
      }
    })
    connection.release();
  })
})

aplicacion.delete("/api/v1/publicaciones/:id", function(req,res){
  pool.getConnection(function(error,connection){
    const email = req.query.email;
    const contrasena = req.query.contrasena;
    const id = req.params.id;
    const query = `select * from autores where email=${connection.escape(email)}`;
    console.log(query);
    console.log(contrasena);
    connection.query(query, function(err,filas,campos){
      if(filas >0){
          if(filas[0].contrasena == contrasena){
            const query2 = `delete from publicaciones where id=${id}`;
            connection.query(query2, function(err,filas,campos){
                res.status(201);
                res.send("La Publicacion se elimino correctamente");
            })
          }
          else{
            res.status(401);
            res.send("contrasena incorrecta");
          }
      }
      else{
        res.status(404);
        res.send("Email no registrado")
      }
    })
    connection.release();
  })
})