const express = require('express')
const aplicacion = express();
var date = new Date();
aplicacion.listen(8080, function () {
    console.log("Servidor iniciado");
})
aplicacion.get("/", function(req,res){
    res.send("Bienvenido");
})
aplicacion.get("/cliente/:id", function(req,res){
    res.send(`Bienvenido cliente ${req.params.id}`);
})
aplicacion.get("/cliente/:id/cuenta/:cuenta", function(req,res){
    res.send(`Bienvenido a la cuenta ${req.params.cuenta} del cliente ${req.params.id} (orden ${req.query.orden})`);
})
