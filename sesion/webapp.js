const express = require("express");
const aplicacion = express();
const session = require("express-session");
aplicacion.use(session({ secret: 'token-muy-secreto', resave: true, saveUninitialized: true }));
aplicacion.listen(8080,function(){
    console.log("Servidor inicado");
});
aplicacion.get("/", function(req, res){
    let visitas = req.session.visitas;
    if(visitas == undefined){
        visitas=0;
    }
    visitas++;
    req.session.visitas = visitas;
    res.send(`Visitas: ${visitas}`);
});
